<?php

    if (isset($_POST['Email']) && isset($_POST['Contraseña']) && isset($_POST['Confirmar-contraseña'])) {
        $email = $_POST['Email'];
        $contraseña = $_POST['Contraseña'];
        $confirmarContraseña = $_POST['Confirmar-contraseña'];

        if (strlen(($contraseña) >= 8) && ($contraseña === $confirmarContraseña)) {
            echo "Datos procesados correctamente";  
        } else {
            echo "Error en el formulario";
        }
    } else {
        echo "Error en el formulario";
    }

?>